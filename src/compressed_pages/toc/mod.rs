mod page_allocation_index;
mod toc_index;

pub use toc_index::TocIndex;

pub use page_allocation_index::PageAllocationIndex;

pub use toc_index::TOC_PAGES_AMOUNT;
